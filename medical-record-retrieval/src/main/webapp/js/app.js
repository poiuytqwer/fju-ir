

var app = angular.module('myApp', ['ngMaterial','ngMessages','ui.bootstrap']);

app.filter('highlight', function($sce) {

	 var stringToArray = function(input) {
     	if(input) {
        	return input.match(/\S+/g);
        } else {
         return [];
        }
     };
     var getRegexPattern = function(keywordArray) {

      var pattern = "(^|\\b)(" + keywordArray.join("|") + ")";
      
      return new RegExp(pattern, "gi");
      
    };

    return function(textToHighlight, keywords) {
    	var filteredText = textToHighlight;
    	console.log(keywords);

      	if(keywords !== "") {
        
        	var keywordList = stringToArray(keywords);
        	var pattern = getRegexPattern(keywordList);
        	//console.log(pattern);
        
        	filteredText = textToHighlight.replace(pattern, '<span class="highlighted">$2</span>');
      	}
      
      return $sce.trustAsHtml(filteredText);
    };

    //return function(text, phrase) {
    //  if (phrase) {
    //  	var index = text.indexOf(phrase);
    //  	if (index > 300) {
    //  		text = text.substring(index);
    //  	}
    //  	text = text.replace(new RegExp('('+phrase+')', 'gi'), '<span class="highlighted">$1</span>');
	//  }
    //  return $sce.trustAsHtml(text)
    //}
 });

// app.filter('highlightNoSub', function($sce) {
//     return function(text, phrase) {
//       if (phrase) {
//       	text = text.replace(new RegExp('('+phrase+')', 'gi'), '<span class="highlighted">$1</span>');
// 	  }
//       return $sce.trustAsHtml(text)
//     }
//  });


app.controller('mainCtrl', ['$scope','recordService','$log','$mdDialog','$mdMedia', function ($scope, recordService, $log, $mdDialog, $mdMedia) {
	 $scope.queryKey={};
	 $scope.queryKey.keyword = 'thyroid';
	 $scope.queryKey.column = 'ALL';
	 $scope.list = [],
	 $scope.filteredTodos = [],
  	 $scope.currentPage = 1,
  	 $scope.numPerPage = 10,
     $scope.maxSize = 5;
     $scope.totalSize = 0;

	$scope.resultIsEmtpy = true;
	$scope.search = function () {
		recordService.search($scope.queryKey).
		then(function(data) {
  			$scope.list = data;
  			$scope.totalSize = data.length;
  			if (data.length>0) {
  				$scope.resultIsEmtpy = false;
  			}
			angular.forEach($scope.list, function(value) {
			  value.index
			});
  			//$log.log($scope.numPages);
			//$log.log($scope.filteredTodos);
  			//$log.log(data);
  			//$log.log($scope.list);
		});
			
	}

    $scope.$watch('list+currentPage', function() {
    	//$log.log($scope.currentPage);
		//$log.log($scope.numPerPage);
		//$log.log($scope.filteredTodos);
	    var begin = (($scope.currentPage - 1) * $scope.numPerPage)
	    , end = begin + $scope.numPerPage;
	    
	    $scope.filteredTodos = $scope.list.slice(begin, end);
  	});

  	$scope.showAdvanced = function(ev, content) {
    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
    $scope.showContent = content;
    console.log($scope.showContent);
    $mdDialog.show({
      controller: DialogController,
      scope: $scope, 
      preserveScope: true,
      templateUrl: 'content.tmpl.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: useFullScreen
    });
 
    $scope.$watch(function() {
      return $mdMedia('xs') || $mdMedia('sm');
    }, function(wantsFullScreen) {
      $scope.customFullscreen = (wantsFullScreen === true);
    });
  };

}]);


function DialogController($scope, $mdDialog) {
  $scope.hide = function() {
    $mdDialog.hide();
  };

  $scope.cancel = function() {
    $mdDialog.cancel();
  };

  $scope.answer = function(answer) {
    $mdDialog.hide(answer);
  };
}

app.service('recordService', function($log, $q, $http) {
    this.search = function(queryKey) {
        var deferred = $q.defer();
        $http({
			method: 'POST',
		 	url: '/search',
		 	data: queryKey,
		}).success(function(data) { 
          	deferred.resolve(data);
        }).error(function(msg, code) {
          	deferred.reject(msg);
          	$log.error(msg, code);
       	});

     	return deferred.promise;
    }

});