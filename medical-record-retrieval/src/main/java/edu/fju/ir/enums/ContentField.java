package edu.fju.ir.enums;

public enum ContentField {
	CLINICAL_HISTORY_AND_DIAGNOSIS("CLINICAL HISTORY AND DIAGNOSIS"),
	CLINICAL_HISTORY("CLINICAL HISTORY"),
	SPECIME_TYPE("SPECIMEN TYPE"),
	SPECIMEN_ADEQUACY("SPECIMEN ADEQUACY"),
	DIAGNOSIS("DIAGNOSIS"),
	GROSS_DESCRIPTION("GROSS DESCRIPTION"),
	MICROSCOPIC_DESCRIPTION("MICROSCOPIC DESCRIPTION"),
	INFECTION("INFECTION");
	
//	PAGE("PAGE");
	
	ContentField(String title) {
		this.title = title;
	}

	private final String title;

	public String getTitle() {
		return title;
	}

	
	public static ContentField valueOfString(String str) {
		for(ContentField v : values())
			
            if(v.getTitle().equalsIgnoreCase(str.replace("\r\n", "").trim())) { 
            	return v;	
            }           	
		return null;
	}
	
}
