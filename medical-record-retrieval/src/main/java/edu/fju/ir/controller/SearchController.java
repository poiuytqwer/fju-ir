package edu.fju.ir.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import edu.fju.ir.bean.MedicalRecord;
import edu.fju.ir.bean.QueryKey;
import edu.fju.ir.service.SearchService;

@RestController
public class SearchController {
	
	@Autowired
	private SearchService searchService;
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public List<MedicalRecord> search(@RequestBody QueryKey queryKey) {
		System.out.println(queryKey);
		return searchService.search(queryKey);
	}
}
