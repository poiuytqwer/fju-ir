package edu.fju.ir.bean;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class MedicalRecord implements Serializable{

	private static final long serialVersionUID = 7156443423783983981L;
	
	private String chrt;
	private String rqno;
	private String seq;
	private String sex;
	private String content;
	private String clinicalHistory;
	private String specimeType;
	private String specimenAdequacy;
	private String diagnosis;
	private String grossDescription;
	private String microscopicDescription;
	
	
	public MedicalRecord(String chrt, String rqno, String seq, String sex, String content) {
		super();
		this.chrt = chrt;
		this.rqno = rqno;
		this.seq = seq;
		this.sex = sex;
		this.content = content;
	}
	
	public MedicalRecord(){
		
	}
	
	public String getChrt() {
		return chrt;
	}
	public void setChrt(String chrt) {
		this.chrt = chrt;
	}
	public String getRqno() {
		return rqno;
	}
	public void setRqno(String rqno) {
		this.rqno = rqno;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getClinicalHistory() {
		return clinicalHistory;
	}

	public void setClinicalHistory(String clinicalHistory) {
		this.clinicalHistory = clinicalHistory;
	}

	public String getSpecimeType() {
		return specimeType;
	}

	public void setSpecimeType(String specimeType) {
		this.specimeType = specimeType;
	}

	public String getSpecimenAdequacy() {
		return specimenAdequacy;
	}

	public void setSpecimenAdequacy(String specimenAdequacy) {
		this.specimenAdequacy = specimenAdequacy;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public String getGrossDescription() {
		return grossDescription;
	}

	public void setGrossDescription(String grossDescription) {
		this.grossDescription = grossDescription;
	}

	public String getMicroscopicDescription() {
		return microscopicDescription;
	}

	public void setMicroscopicDescription(String microscopicDescription) {
		this.microscopicDescription = microscopicDescription;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	@Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MedicalRecord other = (MedicalRecord) obj;
        if (!chrt.equals(other.getChrt()) && !rqno.equals(other.getRqno()))
            return false;
        return true;
    }
	
	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((chrt == null) ? 0 : chrt.hashCode());
        result = prime * result + ((rqno == null) ? 0 : rqno.hashCode());
        return result;
    }
}
