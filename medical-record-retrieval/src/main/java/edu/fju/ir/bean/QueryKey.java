package edu.fju.ir.bean;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class QueryKey implements Serializable {
	
    static final long serialVersionUID = 1L;

    private String keyword;
    private String column;
    private String option;
    
	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}  
}
