package edu.fju.ir.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.beans.factory.annotation.Autowired;

import edu.fju.ir.util.FileUtil;

@WebListener
public class ContextListener implements ServletContextListener {

	@Autowired
	private FileUtil fileUtil;

	public void contextInitialized(ServletContextEvent sce) {
		fileUtil.setUpIndex();
	}

	public void contextDestroyed(ServletContextEvent sce) {
	}

}
