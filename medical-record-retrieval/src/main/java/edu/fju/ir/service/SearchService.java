package edu.fju.ir.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.QueryBuilder;
import org.springframework.stereotype.Service;

import edu.fju.ir.bean.MedicalRecord;
import edu.fju.ir.bean.QueryKey;
import edu.fju.ir.enums.ContentField;
import edu.fju.ir.util.FileUtil;

@Service
public class SearchService {
	public final static String MALIGNANCY = "MALIGNANCY";
	public final static String MALIGNANT = "MALIGNANT";
	public final static String[] NEGATIVE_FOR_MALIGNANCY = { "Negative for malignancy", "No evidence of malignancy",
			"negative for intraepithelial lesion or malignancy", "BENIGN", "no malignancy is noted",
			"No obvious evidence of cytological atypia or malignancy", "There is no evi- dence of malignancy"};
//	public final static String[] NEGATIVE_FOR_MALIGNANCY = { "Negative*malignan", "no*malignan",
//	"BENIGN"};
	final List<String> stopWords = Arrays.asList(
		      "a", "an", "and", "are", "as", "at", "be", "but", "by",
		      "for", "if", "in", "into", "is", "it",
		      "not", "of", "on", "or", "such",
		      "that", "the", "their", "then", "there", "these",
		      "they", "this", "to", "was", "will", "with"
		    );
	final CharArraySet stopSet = new CharArraySet(stopWords, false);


	public List<MedicalRecord> search(QueryKey queryKey) {
		System.out.println("*****************查詢索引開始**********************");
		Directory directory = null;
		DirectoryReader dReader = null;
		List<MedicalRecord> articleList = null;
		String[] keywords = null;
		if (StringUtils.isNotBlank(queryKey.getKeyword())) {
			System.out.println(queryKey.getKeyword());
			keywords = queryKey.getKeyword().split(" ");
		} else {
			return null;
		}

		try {
			directory = FSDirectory.open(FileUtil.path);
			dReader = DirectoryReader.open(directory);
			IndexSearcher searcher = new IndexSearcher(dReader);
			Analyzer analyzer = new StandardAnalyzer();
			QueryBuilder parse = new QueryBuilder(analyzer);
			QueryBuilder simpleParse = new QueryBuilder(new StandardAnalyzer(stopSet));
			BooleanQuery.Builder booleanQuery = new BooleanQuery.Builder();
			if (keywords.length >= 1) {
				for (String str : keywords) {
					str = str.trim();
					Query query = null;
					if (ContentField.CLINICAL_HISTORY.getTitle().equals(queryKey.getColumn())) {
						query = parse.createPhraseQuery(ContentField.CLINICAL_HISTORY.getTitle(), str);
					} else if (ContentField.SPECIME_TYPE.getTitle().equals(queryKey.getColumn())) {
						query = parse.createPhraseQuery(ContentField.SPECIME_TYPE.getTitle(), str);
					} else if (ContentField.SPECIMEN_ADEQUACY.getTitle().equals(queryKey.getColumn())) {
						query = parse.createPhraseQuery(ContentField.SPECIMEN_ADEQUACY.getTitle(), str);
					} else if (ContentField.DIAGNOSIS.getTitle().equals(queryKey.getColumn())) {
						query = parse.createPhraseQuery(ContentField.DIAGNOSIS.getTitle(), str);
					} else if (ContentField.GROSS_DESCRIPTION.getTitle().equals(queryKey.getColumn())) {
						query = parse.createPhraseQuery(ContentField.GROSS_DESCRIPTION.getTitle(), str);
					} else if (ContentField.MICROSCOPIC_DESCRIPTION.getTitle().equals(queryKey.getColumn())) {
						query = parse.createPhraseQuery(ContentField.MICROSCOPIC_DESCRIPTION.getTitle(), str);
					} else {
						query = parse.createPhraseQuery("CONTENT", str.trim());
					}
					if (query != null) {
						booleanQuery.add(query, BooleanClause.Occur.MUST);
					}
				}
			}

			if (queryKey.getOption() != null) {
				String option = queryKey.getOption().substring(0, 6);
				System.out.println(option);				
				if (MALIGNANCY.substring(0, 6).equalsIgnoreCase(option)) {
				//	System.out.println(MALIGNANCY.equalsIgnoreCase(queryKey.getOption()));
					Query queryCY = parse.createPhraseQuery("CONTENT", MALIGNANCY);
					Query queryT = parse.createPhraseQuery("CONTENT", MALIGNANT);
					BooleanQuery.Builder tempQuery = new BooleanQuery.Builder();
					tempQuery.add(queryCY, BooleanClause.Occur.SHOULD);
					tempQuery.add(queryT, BooleanClause.Occur.SHOULD);
					booleanQuery.add(tempQuery.build(), BooleanClause.Occur.MUST);

					for (String str : NEGATIVE_FOR_MALIGNANCY) {
						System.out.println(str);
						Query query2 = parse.createPhraseQuery("CONTENT", str);
						booleanQuery.add(query2, BooleanClause.Occur.MUST_NOT);
					}
				} else {
					BooleanQuery.Builder negativeQuery = new BooleanQuery.Builder();
					for (String str : NEGATIVE_FOR_MALIGNANCY) {
						System.out.println(str);				
						Query query2 = parse.createPhraseQuery("CONTENT", str);
						negativeQuery.add(query2, BooleanClause.Occur.SHOULD);
					}
					booleanQuery.add(negativeQuery.build(), BooleanClause.Occur.MUST);

				}
			}

			if (booleanQuery != null) {
				int count = searcher.count(booleanQuery.build());
				System.out.println(count);
				if (count > 0) {
					TopDocs topDocs = searcher.search(booleanQuery.build(), count);

					if (topDocs != null) {
						articleList = new ArrayList<MedicalRecord>();
						for (int i = 0; i < topDocs.scoreDocs.length; i++) {
							Document doc = searcher.doc(topDocs.scoreDocs[i].doc);
							MedicalRecord record = new MedicalRecord(doc.get("CHRT"), doc.get("RQNO"), doc.get("SEQ"),
									doc.get("SEX"), doc.get("CONTENT"));
							articleList.add(record);
						}
						// System.out.println(articleList.toString());
						System.out.println("共有 " + topDocs.totalHits + " 筆符合條件的紀錄");
					}
				}

			}

			System.out.println("*****************查詢索引結束**********************");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (dReader != null) {
					dReader.close();
				}
				if (directory != null) {
					directory.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return articleList;
	}
}
