package edu.fju.ir.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import edu.fju.ir.bean.MedicalRecord;
import edu.fju.ir.enums.ContentField;

@Component("fileUtil")
public class FileUtil {
	ClassLoader classLoader = getClass().getClassLoader();
	File dataFile = new File(classLoader.getResource("data.xlsx").getFile());
	Analyzer analyzer = new StandardAnalyzer();

	public static final Path path = Paths.get(".", "indexDir");

	private List<MedicalRecord> getExcelContent() {

		//List<MedicalRecord> resultList = new ArrayList<MedicalRecord>();
		Map<MedicalRecord, List<MedicalRecord>> map = new HashMap<MedicalRecord, List<MedicalRecord>>();
		FileInputStream file = null;
		XSSFWorkbook workbook = null;

		try {
			file = new FileInputStream(dataFile);
			workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheetAt(0);
			for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
				XSSFRow row = sheet.getRow(i);
				if (row != null) {
					MedicalRecord record = new MedicalRecord(row.getCell(0).toString(), row.getCell(1).toString(),
							row.getCell(2).toString(), row.getCell(3).toString(), row.getCell(4).toString());
					// System.out.println(record.toString());
					//separateContent(record);
					if (!map.containsKey(record)) {	
						List<MedicalRecord> list = new ArrayList<MedicalRecord>();
						list.add(record);
						map.put(record, list);
					} else {
						map.get(record).add(record);
					//	System.out.println("============list");
					//	System.out.println(map.get(record));
					}
					//list.add(record);
				}
			}
			
			List<MedicalRecord> resultList = combineRecord(map);
			listSeparateContent(resultList);
			return resultList;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (workbook != null) {
				try {
					workbook.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		return null;
	}

	public void createIndexFile() {
		long startTime = System.currentTimeMillis();
		System.out.println("*****************创建索引开始**********************");
		Directory directory = null;
		IndexWriter indexWriter = null;
		try {
			IndexWriterConfig indexWriterConfig = new IndexWriterConfig(analyzer);
			directory = FSDirectory.open(path);
			indexWriter = new IndexWriter(directory, indexWriterConfig);
			indexWriter.deleteAll();

			for (MedicalRecord record : getExcelContent()) {
				//System.out.println(record);
				Document doc = new Document();
				doc.add(new TextField("CHRT", record.getChrt(), Field.Store.YES));
				doc.add(new TextField("RQNO", record.getRqno(), Field.Store.YES));
				doc.add(new TextField("SEQ", record.getSeq(), Field.Store.YES));
				doc.add(new TextField("SEX", record.getSex(), Field.Store.YES));
				doc.add(new TextField("CONTENT", record.getContent(), Field.Store.YES));
				doc.add(new TextField(ContentField.CLINICAL_HISTORY.getTitle(),
						record.getClinicalHistory() == null ? "" : record.getClinicalHistory(), Field.Store.YES));
				doc.add(new TextField(ContentField.SPECIME_TYPE.getTitle(),
						record.getSpecimeType() == null ? "" : record.getSpecimeType(), Field.Store.YES));
				doc.add(new TextField(ContentField.SPECIMEN_ADEQUACY.getTitle(),
						record.getSpecimenAdequacy() == null ? "" : record.getSpecimenAdequacy(), Field.Store.YES));
				doc.add(new TextField(ContentField.DIAGNOSIS.getTitle(),
						record.getDiagnosis() == null ? "" : record.getDiagnosis(), Field.Store.YES));
				doc.add(new TextField(ContentField.GROSS_DESCRIPTION.getTitle(),
						record.getGrossDescription() == null ? "" : record.getGrossDescription(), Field.Store.YES));
				doc.add(new TextField(ContentField.MICROSCOPIC_DESCRIPTION.getTitle(),
						record.getMicroscopicDescription() == null ? "" : record.getMicroscopicDescription(),
						Field.Store.YES));
				indexWriter.addDocument(doc);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (indexWriter != null) {
				try {
					indexWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (directory != null) {
				try {
					directory.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		long endTime = System.currentTimeMillis();
		System.out.println("创建索引文件成功，总共花费" + (endTime - startTime) + "毫秒。");
		System.out.println("*****************创建索引结束**********************");
	}

	public void setUpIndex() {
		File indexFile = new File("./indexDir/");
		if (!indexFile.exists()) {
			getExcelContent();
			createIndexFile();
		}

	}
	private void listSeparateContent(List<MedicalRecord> medicalRecordList) {
		for (MedicalRecord medicalRecord : medicalRecordList) {
			separateContent(medicalRecord);
		}
	}
	

	private void separateContent(MedicalRecord medicalRecord) {
		StringBuilder content = new StringBuilder(medicalRecord.getContent());

		for (ContentField fields : ContentField.values()) {
			if (medicalRecord.getContent().contains("\r\n"+fields.getTitle())) {
				content = content.insert(content.indexOf("\r\n"+fields.getTitle()), "]]");
				// System.out.println("content : " + content);
			}
		}

		String[] sections = content.toString().split("]]");

		for (String str : sections) {
			//int index = str.indexOf(":");
			String[] fields = str.split(":", 2);
			//System.out.println(fields[0].replace("\r\n", ""));
			if (ContentField.valueOfString(fields[0]) != null) {
				//String s = fields[0].replace("\r\n", "").trim();
				switch (ContentField.valueOfString(fields[0])) {
				case CLINICAL_HISTORY:
					medicalRecord.setClinicalHistory(fields[1]);
					break;
				case CLINICAL_HISTORY_AND_DIAGNOSIS:
					medicalRecord.setClinicalHistory(fields[1]);
					break;
				case DIAGNOSIS:
					medicalRecord.setDiagnosis(fields[1]);
					break;
				case GROSS_DESCRIPTION:
					medicalRecord.setGrossDescription(fields[1]);
					break;
				case MICROSCOPIC_DESCRIPTION:
					medicalRecord.setMicroscopicDescription(fields[1]);
					break;
				case SPECIMEN_ADEQUACY:
					medicalRecord.setSpecimenAdequacy(fields[1]);
					break;
				case SPECIME_TYPE:
					medicalRecord.setSpecimeType(fields[1]);
					break;
				default:
					break;
				}
			}
		}
		//System.out.println("============medicalRecord============" + medicalRecord);
	}
	
	private List<MedicalRecord> combineRecord(Map<MedicalRecord, List<MedicalRecord>> map) {
		List<MedicalRecord> list = new ArrayList<MedicalRecord>();
		for (MedicalRecord record : map.keySet()) {
			if (map.get(record)!=null && map.get(record).size()>1) {
				sortBySeq(map.get(record));
				//System.out.println("============sortList=============");
				//System.out.println(map.get(record));
				StringBuilder firstContent = new StringBuilder(map.get(record).get(0).getContent());
				for (int i = 1; i < map.get(record).size(); i++) {
					firstContent.append(map.get(record).get(i).getContent());
				}
				map.get(record).get(0).setContent(firstContent.toString());
				//System.out.println("============first=============");
				//System.out.println(map.get(str).get(0));
				list.add(map.get(record).get(0));
			} else {
				list.addAll(map.get(record));
			}
		}
		return list;
	}
	
	private void sortBySeq(List<MedicalRecord> list){
		Collections.sort(list, new Comparator<MedicalRecord>() {
	        @Override
			public int compare(MedicalRecord record1, MedicalRecord record2) {
				return record1.getSeq().compareTo(record2.getSeq());
			}
	    });
	}
	
	public static void main(String[] args) {
		FileUtil fileUtil = new FileUtil();
		// fileUtil.setUpIndex();
		fileUtil.getExcelContent();
		// System.out.println(Paths.get(".", "indexDir").toString());
		//
		// fileUtil.createIndexFile();
		//
		// SearchService service = new SearchService();
		// service.search("Uterus");
	}

}
